// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Castle.generated.h"

UCLASS()
class DRAGONENDEAVORS_API ACastle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACastle();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    
    void Death();
    float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);
    

    
protected:
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Castle)
    UStaticMeshComponent* CastleMesh;
    
    UPROPERTY(EditAnywhere)
    TSubclassOf<AActor> TreasureClass;

private:
    
    float Health=10.0f;
    FTimerHandle DeathHandle;
    

	
};
