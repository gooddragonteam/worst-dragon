// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "Treasure.h"
#include "Dragon.h"

// Sets default values
ATreasure::ATreasure()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TreasureMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TreasureMesh"));

	box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	RootComponent = TreasureMesh;
	box->SetBoxExtent(FVector(100.f, 100.f, 100.f));
	box->SetupAttachment(RootComponent);

	box->OnComponentBeginOverlap.AddDynamic(this, &ATreasure::OnOverlapBegin);

}

// Called when the game starts or when spawned
void ATreasure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATreasure::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    
}

void ATreasure::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor,
class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (OtherActor->IsA<ADragon>()) {
		//dragon picked up the treasure
		(Cast<ADragon>(UGameplayStatics::GetPlayerPawn(this, 0)))->addToScore(mValue);
		Super::Destroy();
	}

}


