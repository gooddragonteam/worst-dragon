// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "Dragon.h"


// Sets default values
ADragon::ADragon()
{
	depositedMoney = 0;
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(180.0f, 55.0f);
	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);


	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 180.f;
	CameraBoom->bUseControllerViewRotation = false;
	CameraBoom->RelativeRotation = FRotator(0.f, -90.f, 0.f);
	CameraBoom->bEnableCameraLag = true;
	CameraBoom->CameraRotationLagSpeed = 3.f;

	// Create the second camera boom
	CameraBoomFP = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoomFP"));
	CameraBoomFP->SetupAttachment(RootComponent);
	CameraBoomFP->TargetArmLength = 180.f;
	CameraBoomFP->bUseControllerViewRotation = false;
	CameraBoomFP->RelativeRotation = FRotator(0.f, 0.f, 0.f);
	CameraBoomFP->RelativeLocation = FVector(100.0f, 0.f, 50.0f);
	CameraBoomFP->bEnableCameraLag = false;
	CameraBoomFP->CameraRotationLagSpeed = 3.f;


	// Create two cameras...
	FollowCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	FollowCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);

	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	
	FirstPersonCameraComponent->RelativeLocation = FVector(100.0f, 0.f, 50.0f);
	FirstPersonCameraComponent->RelativeRotation = FRotator(0.f, 0.f, 0.f);
	FirstPersonCameraComponent->SetupAttachment(CameraBoomFP, USpringArmComponent::SocketName);


	score = 0;
	health = 1000.0f;
}

// Called when the game starts or when spawned
void ADragon::BeginPlay()
{
	Super::BeginPlay();

}

int ADragon::getScore() { 
	return score; 
}

float ADragon::getHealth() { 
	return health; 
}

void ADragon::takeDamage(float damage) { 
	health -= damage;
    if(score >= 10) score-=10;
}

void ADragon::gainHP(float hp) {
	health += hp;
}

void ADragon::addToScore(int change) { 
	score += change; 
}

void ADragon::depositMoney(){
	depositedMoney += score;
	score = 0;
}

int ADragon::getMoney() {
	return depositedMoney;
}