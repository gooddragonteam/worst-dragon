// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "DragonController.generated.h"

/**
*
*/
UCLASS()
class DRAGONENDEAVORS_API ADragonController : public APlayerController
{
	GENERATED_BODY()
public:
	ADragonController();

	void MoveForward(float value);
	void TurnRight(float value);

	void Flap();
	void Glide(float value);

	void Breathe();
	void StopBreathing();

	UAudioComponent* PlaySound(class USoundCue* Sound);
protected:
	// Begin PlayerController interface
	//virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface
	UPROPERTY(Transient)
	class UParticleSystemComponent* firePC;
	UPROPERTY(Transient)
	class UAudioComponent* FireAC;
	float fireTickTime = .1f;
	FTimerHandle fireTickHandle;
	float fireRange = 1500.0f;
	float fireDamage = 20.f;

	void FireHit();
};
