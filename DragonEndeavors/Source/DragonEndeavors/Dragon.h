// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Dragon.generated.h"

UCLASS()
class DRAGONENDEAVORS_API ADragon : public ACharacter
{
	GENERATED_BODY()
		/** Follow camera */
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCameraComponent;
	
	// First Person Camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FirstPersonCameraComponent;

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Camera boom positioning the camera before the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoomFP;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	class UParticleSystem* fireFX;

	int score;
	int depositedMoney;
	//arbitrary starting health value
	float health;
public:
	// Sets default values for this character's properties
	ADragon();
	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCameraComponent() const { return FollowCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UParticleSystem* getFireParticles() { return fireFX; }
	
	UFUNCTION(BlueprintCallable, Category = "AnyString")
		int getScore();
	
	UFUNCTION(BlueprintCallable, Category = "AnyString")
		float getHealth();
	UFUNCTION(BlueprintCallable, Category = "AnyString")
		int getMoney();
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	class USoundCue* FireRoarSound;
	void addToScore(int change);
    void OnPickup(); 
	void depositMoney();
	void takeDamage(float damage);
	void gainHP(float hp);
};
