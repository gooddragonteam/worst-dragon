// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "SpawnManagerCastle.h"


// Sets default values
ASpawnManagerCastle::ASpawnManagerCastle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
//	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawnManagerCastle::BeginPlay()
{
    Super::BeginPlay();
    FTimerHandle Timer;
    GetWorldTimerManager().SetTimer(Timer, this, &ASpawnManagerCastle::OnSpawnTimer,
                                     SpawnTime, true);
    
}

// Called every frame
void ASpawnManagerCastle::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    
}

void ASpawnManagerCastle::OnSpawnTimer(){
    if(SpawnTime <= 400.0f){
        SpawnTime+=5.0f; // spawn the castles less each time
        int elements =  SpawnPoints.Num();
        if (elements) {
            int index = FMath::RandRange(0, elements - 1);
            FVector pos = SpawnPoints[index]->GetActorLocation();
            FRotator rot = SpawnPoints[index]->GetActorRotation();
            // Spawn an AActor of subclass ActorClass, at specified position and rotation
            AActor* Actor = GetWorld()->SpawnActor<AActor>(ActorClass, pos, rot);
        }
    }
}

