// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "Castle.h"
#include "Dragon.h"
#include "Treasure.h"

// Sets default values
ACastle::ACastle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    CastleMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CastleMesh"));
    RootComponent = CastleMesh;
}

// Called when the game starts or when spawned
void ACastle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACastle::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    if(Health <= 0) Death();

}

//void ACastle::SpawnTreasure(){
//    AActor* Actor = GetWorld()->SpawnActor<AActor>(TreasureClass);
//
//}

void ACastle::Death(){
	//spawn treasure here
	GetWorld()->SpawnActor<AActor>(TreasureClass, GetActorLocation(), GetActorRotation());

    Super::Destroy();
    
}

float ACastle::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
                                           EventInstigator, DamageCauser);
    if (ActualDamage > 0.0f)
    {
        Health -= ActualDamage;
        if (Health <= 0.0f)
        {
            // We're dead, don't allow further damage
            bCanBeDamaged = false;
            Death();
            // TODO: Process death
//            GetController()->PawnPendingDestroy(this);
//            GetWorldTimerManager().SetTimer(DeathHandle, this, &AVillager::OnDeath, PlayAnimMontage(DeathAnim)-0.25f);
            
        }

    }
    return ActualDamage;
}

