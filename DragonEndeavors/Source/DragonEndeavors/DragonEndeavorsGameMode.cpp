// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "DragonEndeavorsGameMode.h"
#include "DragonController.h"

ADragonEndeavorsGameMode::ADragonEndeavorsGameMode() {
	// use our custom PlayerController class
	PlayerControllerClass = ADragonController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/BP_Dragon"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}


