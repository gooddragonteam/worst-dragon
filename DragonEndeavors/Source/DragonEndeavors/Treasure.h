// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Treasure.generated.h"

UCLASS()
class DRAGONENDEAVORS_API ATreasure : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATreasure();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor,
	class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	
protected:
	//according to unreal documentation this is how to do this??
	UPROPERTY(VisibleAnywhere, Category = "Switch Components")
	class UBoxComponent* box;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Castle)
		UStaticMeshComponent* TreasureMesh;

    int mValue = 500;
	
};
