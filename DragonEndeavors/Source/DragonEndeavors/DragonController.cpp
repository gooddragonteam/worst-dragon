// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "DragonController.h"
#include "Dragon.h"
#include "Soldier.h"
#include "Villager.h"
#include "Castle.h"

ADragonController::ADragonController() {

}

void ADragonController::SetupInputComponent() {
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForward", this, &ADragonController::MoveForward);
	InputComponent->BindAxis("TurnRight", this, &ADragonController::TurnRight);
	InputComponent->BindAxis("Glide", this, &ADragonController::Glide);
	InputComponent->BindAction("Flap", IE_Pressed, this, &ADragonController::Flap);
	InputComponent->BindAction("Breathe", IE_Pressed, this, &ADragonController::Breathe);
	InputComponent->BindAction("Breathe", IE_Released, this, &ADragonController::StopBreathing);
}


void ADragonController::MoveForward(float value) {
	if (value != 0.0f)
	{
		APawn* pawn = GetPawn();
		if (pawn != nullptr)
		{
			pawn->AddMovementInput(GetActorForwardVector(), value);
		}
	}
}
void ADragonController::TurnRight(float value) {
	if (value != 0.0f)
	{
		APawn* pawn = GetPawn();
		if (pawn != nullptr)
		{
			pawn->AddControllerYawInput(value);
		}
	}
}
void ADragonController::Flap() {
	APawn* pawn = GetPawn();
	if (pawn != nullptr)
	{
		//pawn->GetMovementComponent()->AddInputVector(GetActorUpVector()*10000000.f, true);
		pawn->LaunchPawn(GetActorUpVector()*400.f, false, true);
	}
}
void ADragonController::Glide(float value) {
	APawn* pawn = GetPawn();
	if (pawn != nullptr && value > 0.000001f)
	{
		float fallSpeed = pawn->GetMovementComponent()->Velocity.Z;
		if (fallSpeed < 0.f) {
			pawn->GetMovementComponent()->Velocity.Z = -.1f;
		}
	}
}

void ADragonController::Breathe() {
	APawn* pawn = GetPawn();
	if (pawn != nullptr) {
		FireAC = PlaySound(((ADragon*)pawn)->FireRoarSound);
		firePC = UGameplayStatics::SpawnEmitterAttached(((ADragon*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))->getFireParticles(), UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetMesh(), TEXT("FireSocket"));
		firePC->RelativeRotation = pawn->GetActorRotation();
		//firePC->RelativeRotation.Yaw = -90.f;
		GetWorldTimerManager().SetTimer(fireTickHandle, this, &ADragonController::FireHit, fireTickTime, true);
	}
}

void ADragonController::StopBreathing() {
	GetWorldTimerManager().ClearTimer(fireTickHandle);
	if (firePC != nullptr) firePC->DeactivateSystem();
	if (FireAC != nullptr) FireAC->Stop();
}

void ADragonController::FireHit() {
	APawn* pawn = GetPawn();
	if (pawn != nullptr) {
		FVector StartPos = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetMesh()->GetSocketLocation("FireSocket");
		FVector dragonForward = pawn->GetActorForwardVector();
		FVector dragonDown = -1.f * pawn->GetActorUpVector();
		FVector EndPos = StartPos + (dragonForward*fireRange) + (dragonDown*100.f);
		// Perform trace to retrieve hit info
		FCollisionQueryParams TraceParams(FName("FireSocket"), true, Instigator);
		TraceParams.bTraceAsyncScene = true;
		TraceParams.bReturnPhysicalMaterial = true;
		// This fires the ray and checks against all objects w/ collision
		FHitResult Hit(ForceInit);
		GetWorld()->LineTraceSingleByObjectType(Hit, StartPos, EndPos,
			FCollisionObjectQueryParams::AllObjects, TraceParams);
		// Did this hit anything?
		if (Hit.bBlockingHit)
		{
			AActor* hitee = Hit.GetActor();
			if (hitee->IsA<ASoldier>())
			{
				((ASoldier*)hitee)->TakeDamage(fireDamage, FDamageEvent(), this, pawn);
			}
			else if (hitee->IsA<AVillager>())
			{
				((AVillager*)hitee)->TakeDamage(fireDamage, FDamageEvent(), this, pawn);
			}
			else if (hitee->IsA<ACastle>())
			{
				((ACastle*)hitee)->TakeDamage(fireDamage, FDamageEvent(), this, pawn);
			}
		}
	}
}

UAudioComponent* ADragonController::PlaySound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, GetPawn()->GetRootComponent());
	}
	return AC;
}