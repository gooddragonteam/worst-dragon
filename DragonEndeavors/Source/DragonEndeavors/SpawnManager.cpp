// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "SpawnManager.h"
#include "Engine/TargetPoint.h"

// Sets default values
ASpawnManager::ASpawnManager()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
//    PrimaryActorTick.bCanEv erTick = true;
    
}

// Called when the game starts or when spawned
void ASpawnManager::BeginPlay()
{
    Super::BeginPlay();
    
    FTimerHandle handle;
    GetWorldTimerManager().SetTimer(handle, this, &ASpawnManager::OnSpawnTimer, SpawnTime, true);
}

// Called every frame
void ASpawnManager::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    
}

void ASpawnManager::OnSpawnTimer()
{
    if(SpawnTime <= 400.0f){
        SpawnTime+=5.0f;
        int Num = SpawnPoints.Num();
        if (Num) {
            int index = FMath::RandRange(0, Num - 1);
            FVector pos = SpawnPoints[index]->GetActorLocation();
            FRotator rot = SpawnPoints[index]->GetActorRotation();
        
            // Spawn an ACharacter of subclass CharacterClass, at specified position and rotation
            ACharacter* Char = GetWorld()->SpawnActor<ACharacter>(CharacterClass, pos, rot);
            if (Char)
            {
                // Spawn the AI controller for the character
                Char->SpawnDefaultController();
            }
    
        }
    }
}

