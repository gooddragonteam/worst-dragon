// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "SoldierAIController.generated.h"

/**
 * 
 */
UCLASS()
class DRAGONENDEAVORS_API ASoldierAIController : public AAIController
{
	GENERATED_BODY()
public:
	ASoldierAIController();
	void BeginPlay() override;
	void Tick(float DeltaSeconds) override;
	void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) override;

	void whenIsBurnt(AActor* damageCauser);

	// Run
	void StartRun(FVector away);
	void Stop();
	// Wander
	void Wander();
	// Hit&Run
	void KeepDistance(float dist);
	void RestartChase();
protected:
	enum State { Start, Chase, HitAndRun, Attack, Dead, Wandering, Run, Approaching, Waiting};
	State state;
	// Ranges
	UPROPERTY(EditDefaultsOnly, Category = Damage)
		float AttackRange = 1500.0f;
	UPROPERTY(EditDefaultsOnly, Category = Damage)
		float GuardianRange = 3000.0f;
	UPROPERTY(EditDefaultsOnly, Category = Damage)
		float HitAndRunRange = 500.0f;

	FTimerHandle StopHandle;
	FTimerHandle RestartChaseHandle;

	
};
