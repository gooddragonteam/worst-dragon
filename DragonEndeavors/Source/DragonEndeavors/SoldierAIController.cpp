// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "SoldierAIController.h"
#include "Soldier.h"
#include <iostream>

ASoldierAIController::ASoldierAIController() {
	
}

void ASoldierAIController::BeginPlay() {
	Super::BeginPlay();
	state = Start;
	
}

void ASoldierAIController::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);
	FVector enemyloc = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
	FVector myloc = GetPawn()->GetActorLocation();
	float distance = FVector::Dist(enemyloc, myloc);
	float hp = (Cast<ASoldier>(GetPawn()))->GetHealth();
	// Low hp, run!
	if (hp <= 50.0f && distance <= HitAndRunRange) {
		if(state != Run)
			StopMovement();
		if(state == Approaching || state == Attack || state == HitAndRun)
			(Cast<ASoldier>(GetPawn()))->StopAttack();
		state = Run;
		StartRun(UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation());
	}
	// Other situations when out of guardian range
	if (state == Start || state == Wandering) {
		if (GetCharacter())
			GetCharacter()->GetCharacterMovement()->bOrientRotationToMovement = true;
		// Chase or run
		if (distance <= GuardianRange) {
			StopMovement();
			GetWorldTimerManager().ClearTimer(StopHandle);
			// Have enough hp, chase
			if (hp > 50) {
				state = Chase;
     			MoveToActor(UGameplayStatics::GetPlayerPawn(this, 0));
			}
		}
		// Wander
		else {
			if (state == Start) {
				
				state = Wandering;
				if (FMath::FRandRange(0, 10) > 4.0f) {
					Wander();
					//If got stuck, reset to start state
					GetWorldTimerManager().SetTimer(StopHandle, this, &ASoldierAIController::Stop, 0.5f);
				}
				else {
					GetWorldTimerManager().SetTimer(StopHandle, this, &ASoldierAIController::Stop, 1.0f);
				}
			}
		}
	}
	else if (state == Chase) {
		if (GetCharacter())
			GetCharacter()->GetCharacterMovement()->bOrientRotationToMovement = true;
		MoveToLocation(FVector(enemyloc.X, enemyloc.Y, 0));
		// Within atk range
		if (distance < AttackRange) {
			(Cast<ASoldier>(GetPawn()))->StartAttack();
			
			state = Approaching;
			if (GetCharacter())
				GetCharacter()->GetCharacterMovement()->bOrientRotationToMovement = false;
		}
		else if (distance > GuardianRange) {
			Stop();
		}
	}
	else if (state == Attack) {
		GetPawn()->FaceRotation((enemyloc - myloc).Rotation());
		if (distance > AttackRange) {
  			(Cast<ASoldier>(GetPawn()))->StopAttack();
			
			state = Waiting;
			GetWorldTimerManager().SetTimer(RestartChaseHandle, this, &ASoldierAIController::RestartChase, 3.0f);
			std::cout << "Stop Attacking" << std::endl;
		}
		else if (distance <= HitAndRunRange) {
			if (GetCharacter())
				GetCharacter()->GetCharacterMovement()->bOrientRotationToMovement = false;
			KeepDistance(HitAndRunRange);
			
			state = HitAndRun;
		}
	}
	else if (state == HitAndRun) {
		// Out of hit and run range
		if (distance > HitAndRunRange) {
			StopMovement();
			
  			state = Attack;
			std::cout << "Stop and Attack" << std::endl;
			
		}
		// Keep H&R
		else {
			StopMovement();
			if (GetCharacter())
				GetCharacter()->GetCharacterMovement()->bOrientRotationToMovement = false;
			KeepDistance(HitAndRunRange);
		}
		
	}
	else if (state == Approaching) {
		GetPawn()->FaceRotation((enemyloc - myloc).Rotation());
		// Within Range, h&r
		if (distance <= HitAndRunRange) {
			StopMovement();
			KeepDistance(HitAndRunRange);
			
			state = HitAndRun;
		}
		else if (distance > AttackRange) {
			(Cast<ASoldier>(GetPawn()))->StopAttack();
			
			state = Waiting;
			GetWorldTimerManager().SetTimer(RestartChaseHandle, this, &ASoldierAIController::RestartChase, 3.0f);
		}
	}
	else if (state == Run) {
		if (GetCharacter())
			GetCharacter()->GetCharacterMovement()->bOrientRotationToMovement = true;
		// Dragon out of range, stop running
		if (distance > GuardianRange) {
			Stop();
		}
	}
}

void ASoldierAIController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) {
	
	if (Result == EPathFollowingResult::Success && (state == Wandering||state == Run)) {
		
		state = Start;
		// Didn't get stuck so pause the reset timer
		GetWorldTimerManager().PauseTimer(StopHandle);
	}
}

void ASoldierAIController::whenIsBurnt(AActor* damageCauser) {
	if ((Cast<ASoldier>(GetPawn()))->GetHealth()) {
		(Cast<ASoldier>(GetPawn()))->StopAttack();
		if(state != Run)
		state = Run;
		StartRun(damageCauser->GetActorLocation());
	}
}


void ASoldierAIController::StartRun(FVector away)
{
	if (GetPawn()) {
		
		FVector dir = GetPawn()->GetActorLocation() - away;
		dir.Normalize();
		FVector direction = FVector(dir.X, dir.Y, 0);
		FVector dest = GetPawn()->GetActorLocation() + direction * 500.0f;
		MoveToLocation(dest);
	}
}

void ASoldierAIController::Stop()
{
	StopMovement();
	
	state = Start;
	
}

void ASoldierAIController::Wander()
{
	if (GetPawn()) {
		FVector loc = GetPawn()->GetActorLocation();
		FVector dirVector;
		dirVector.X = FMath::FRandRange(-500, 500);
		dirVector.Y = FMath::FRandRange(-500, 500);
		dirVector.Z = 0;
		FVector dest = loc + dirVector;
		MoveToLocation(dest);
		
		state = Wandering;
	}
}

void ASoldierAIController::KeepDistance(float dist) {
	if (GetPawn()) {
		FVector myloc = GetPawn()->GetActorLocation();
		FVector dragonloc = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
		FVector dir = myloc - dragonloc;
		dir.Normalize();
		FVector direction = FVector(dir.X, dir.Y, 0);
		direction.Normalize();
		GetPawn()->FaceRotation((dragonloc - myloc).Rotation());
		MoveToLocation(dragonloc + direction * dist);
	}
}

void ASoldierAIController::RestartChase() {
  	FVector myloc = GetPawn()->GetActorLocation();
	FVector dragonloc = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
	if (FVector::Dist(myloc, dragonloc) <= GuardianRange) {
		state = Chase;
		MoveToActor(UGameplayStatics::GetPlayerPawn(this, 0));
	}
	else {
		state = Start;
	}
}
