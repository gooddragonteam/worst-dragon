// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "HUD_dragon.generated.h"

/**
 * 
 */
UCLASS()
class DRAGONENDEAVORS_API UHUD_dragon : public UUserWidget
{
	GENERATED_BODY()
    
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD_Dragon")
    FString hud_dragon;
	virtual void Tick_Implementation(FGeometry MyGeometry, float InDeltaTime) override; 
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD_Dragon")
		FString score;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD_Dragon")
		float health;
};
