// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "SoldierSpawner.generated.h"

UCLASS()
class DRAGONENDEAVORS_API ASoldierSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASoldierSpawner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void spawnSoldier();

	void onSpawnTimer();

	float soldierRespawnTime = 8.0f;
	
	UPROPERTY(EditAnywhere)
		TSubclassOf<ACharacter> CharacterClass;

	UPROPERTY(EditAnywhere)
		TArray<class ATargetPoint*> SpawnPoints;
};
