// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "SpawnManagerCastle.generated.h"

UCLASS()
class DRAGONENDEAVORS_API ASpawnManagerCastle : public AActor
{
	GENERATED_BODY()
    
public:
    // Sets default values for this actor's properties
    ASpawnManagerCastle();
    
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    // Called every frame
    virtual void Tick( float DeltaSeconds ) override;
    
    void OnSpawnTimer();
    
protected:
    UPROPERTY(EditAnywhere)
    TArray<class ATargetPoint*> SpawnPoints;
    
    UPROPERTY(EditAnywhere)
    TSubclassOf<AActor> ActorClass;
    
    UPROPERTY(EditAnywhere)
    float SpawnTime = 10.0f;

	
};
