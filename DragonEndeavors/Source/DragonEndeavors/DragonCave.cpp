// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "DragonCave.h"
#include "Dragon.h"

// Sets default values
ADragonCave::ADragonCave()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CaveMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CaveMesh"));

	box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	RootComponent = CaveMesh;
	box->SetBoxExtent(FVector(100.f,100.f,100.f));
	box->SetupAttachment(RootComponent);

	box->OnComponentBeginOverlap.AddDynamic(this, &ADragonCave::OnOverlapBegin);
}

// Called when the game starts or when spawned
void ADragonCave::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADragonCave::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ADragonCave::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor,
	class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (OtherActor->IsA<ADragon>()) {
		((ADragon*)OtherActor)->depositMoney();
	}

}
