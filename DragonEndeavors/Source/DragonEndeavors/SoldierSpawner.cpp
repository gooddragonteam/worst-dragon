// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "SoldierSpawner.h"
#include "DragonEndeavorsGameMode.h"


// Sets default values
ASoldierSpawner::ASoldierSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASoldierSpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASoldierSpawner::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if (((ADragonEndeavorsGameMode*)UGameplayStatics::GetGameMode(GetWorld()))->soldierDead) {
		((ADragonEndeavorsGameMode*)UGameplayStatics::GetGameMode(GetWorld()))->soldierDead = false;
		spawnSoldier();
	}
}

void ASoldierSpawner::spawnSoldier() {
	FTimerHandle handle;
	GetWorldTimerManager().SetTimer(handle, this, &ASoldierSpawner::onSpawnTimer, soldierRespawnTime);
}

void ASoldierSpawner::onSpawnTimer() {
	int Num = SpawnPoints.Num();
	if (Num) {
		int index = FMath::RandRange(0, Num - 1);
		FVector pos = SpawnPoints[index]->GetActorLocation();
		FRotator rot = SpawnPoints[index]->GetActorRotation();

			// Spawn an ACharacter of subclass CharacterClass, at specified position and rotation
		ACharacter* Char = GetWorld()->SpawnActor<ACharacter>(CharacterClass, pos, rot);
		if (Char)
		{
			// Spawn the AI controller for the character
			Char->SpawnDefaultController();
		}

	}
}